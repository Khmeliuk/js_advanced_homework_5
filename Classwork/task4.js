/*

  Задание "Шифр цезаря":

    https://uk.wikipedia.org/wiki/%D0%A8%D0%B8%D1%84%D1%80_%D0%A6%D0%B5%D0%B7%D0%B0%D1%80%D1%8F

    Написать функцию, которая будет принимать в себя слово и количество
    симовлов на которые нужно сделать сдвиг внутри.

    Написать функцию дешефратор которая вернет слово в изначальный вид.

    Сделать статичные функции используя bind и метод частичного
    вызова функции (каррирования), которая будет создавать и дешефровать
    слова с статично забитым шагом от одного до 5.


    Например:
      encryptCesar( 3, 'Word');
      encryptCesar1(...)
      ...
      encryptCesar5(...)

      decryptCesar1(3, 'Sdwq');
      decryptCesar1(...)
      ...
      decryptCesar5(...)

      "а".charCodeAt(); // 1072
      String.fromCharCode(189, 43, 190, 61) // ½+¾

*/

function encryptCesar(word, shift) {
  //перебор строки
  let decodedWord = "";
  for (let i = 0; i < word.length; i++) {
    let char = word[i];
    let charCode = word[i].charCodeAt();
    let decodedCode = charCode +shift;
    let decodedChar = String.fromCharCode(decodedCode);
    decodedWord += decodedChar;
  }
  return decodedWord;
}

let codedWord = encryptCesar("Проба пера", 3);
let decodedWord = encryptCesar(codedWord, -3);
//let word = decodedEncryptCesar(decodedWord, 3);
console.log(codedWord);
console.log(decodedWord);


