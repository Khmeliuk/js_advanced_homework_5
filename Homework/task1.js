/*

  Задание:

    1. Написать конструктор объекта комментария который принимает 3 аргумента
    ( имя, текст сообщения, ссылка на аватаку);

    {
      name: '',
      text: '',
      avatarUrl: '...jpg'
      likes: 0
    }
      + Создать прототип, в котором будет содержаться ссылка на картинку по умлочанию
      + В прототипе должен быть метод который увеличивает счетик лайков

    var myComment1 = new Comment(...);

    2. Создать массив из 4х комментариев.
    var CommentsArray = [myComment1, myComment2...]

    3. Созадть функцию конструктор, которая принимает массив коментариев.
      И выводит каждый из них на страничку.

    <div id="CommentsFeed"></div>


*/

let comment = {
  name: "Katya",
  text : "myComment",
  avatarUrl : "default url"
  ,getLikes
}

function getLikes(){
  this.likes = "Like default";
}

function Comment(name, text, avatarUrl){
  this.name= name;
  this.text= text;
  if(this.avatarUrl !== undefined){
    this.avatarUrl = avatarUrl;
  }
  // this.likes= likes;

  Object.setPrototypeOf(this, comment);
}

let comment1 = new Comment("Vitya", "comment1", "Like1");
let comment2 = new Comment("Vasya", "comment2", "Like1");
let comment3 = new Comment("Masha", "comment3", "Like1");
let comment4 = new Comment("Pasha", "comment4", "Like1");
console.log(comment1.getLikes);

let CommentsArray = [comment1, comment2, comment3, comment4];

function printComents(){
  let h1 = document.createElement("h1");
  h1.innerText = "I know how binding works in JS";
document.body.appendChild(h1);
}
